
function getAge(date) { //Convertir la date de naissance en âge.

	var diff = Date.now() - new Date(date).getTime();
	var age = new Date(diff);
	return Math.abs(age.getUTCFullYear() - 1970);
}

function annuler() {//Vider le formulaire
	document.getElementById("nom").value = "";
	document.getElementById("prenom").value = "";
	document.getElementById("dateNaissance").value = "";
	document.getElementById("num").value = "";
}


let toggle = () => {// faire apparaitre le bouton "éditer"

	let element = document.getElementById("edite");
	let hidden = element.getAttribute("hidden");

	if (hidden) {
		element.setAttribute("hidden", "hidden");

	} else {
		element.removeAttribute("hidden");
	}
}
let toggle2 = () => {//faire disparaitre le bouton "éditer"

	let element = document.getElementById("edite");
	let hidden = element.getAttribute("hidden");

	if (hidden) {
		element.removeAttribute("hidden");

	} else {
		element.setAttribute("hidden", "hidden");

	}
}


function sortTable(c, typeDeTri) {//Tri du tableau "c" défini  le nom de champ et "typeDeTri" defini le type de tri.
	let rows, switching, i, x, y, shouldSwitch;
	switching = true;
	if (typeDeTri == 1) {//tri croissant 
		while (switching) {
			switching = false;
			let rows = table.rows;
			for (i = 1; i < (rows.length - 1); i++) {
				shouldSwitch = false;
				x = rows[i].getElementsByTagName("TD")[c];
				y = rows[i + 1].getElementsByTagName("TD")[c];
				if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
					shouldSwitch = true;
					break;
				}
			}
			if (shouldSwitch) {
				rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
				switching = true;

			}
		}
	}
	if (typeDeTri == 0) { //trie descoissant
		while (switching) {
			switching = false;
			rows = table.rows;
			for (i = 1; i < (rows.length - 1); i++) {
				shouldSwitch = false;
				x = rows[i].getElementsByTagName("TD")[c];
				y = rows[i + 1].getElementsByTagName("TD")[c];
				if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
					shouldSwitch = true;
					break;
				}
			}
			if (shouldSwitch) {
				rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
				switching = true;

			}
		}

	}
}


const url = "http://localhost:3000";

async function fetchListe() {//afficher mon base de donnée
	const response = await fetch(url + '/liste');
	const liste = await response.json();
	document.getElementById("tab").innerHTML = "";
	let table = document.createElement('table');
	let thead = document.createElement('thead');
	let tbody = document.createElement('tbody');

	table.appendChild(thead);
	table.appendChild(tbody);
	document.getElementById('tab').appendChild(table);
	let row_1 = document.createElement('tr');
	let heading_1 = document.createElement('th');
	heading_1.innerHTML = `nom<span onclick=sortTable(0,1)>&#9660;</span><span onclick=sortTable(0,0)>&#9650;</span>`;
	let heading_2 = document.createElement('th');
	heading_2.innerHTML = `Prenom<span onclick=sortTable(1,1)>&#9660;</span><span onclick=sortTable(1,0)>&#9650;</span>`;
	let heading_3 = document.createElement('th');
	heading_3.innerHTML = `Age<span onclick=sortTable(2,1)>&#9660;</span><span onclick=sortTable(2,0)>&#9650;</span>`;
	let heading_4 = document.createElement('th');
	heading_4.innerHTML = `Date de Naissance<span onclick=sortTable(3,1)>&#9660;</span><span onclick=sortTable(3,0)>&#9650;</span>`;
	let heading_5 = document.createElement('th');
	heading_5.innerHTML = `Numéro de la carte d'identité nationale<span onclick=sortTable(4,1)>&#9660;</span><span onclick=sortTable(4,0)>&#9650;</span>`;
	let heading_6 = document.createElement('th');
	heading_6.innerHTML = "Action";

	row_1.appendChild(heading_1);
	row_1.appendChild(heading_2);
	row_1.appendChild(heading_3);
	row_1.appendChild(heading_4);
	row_1.appendChild(heading_5);
	row_1.appendChild(heading_6);
	thead.appendChild(row_1);
	for (let i = 0; i < liste.model.length; i++) {
		let row_2 = document.createElement('tr');
		let cellNom = document.createElement('td');
		cellNom.innerHTML = liste.model[i].nom;
		let cellPrenom = document.createElement('td');
		cellPrenom.innerHTML = liste.model[i].prenom;
		let cellDateNaissance = document.createElement('td');
		cellDateNaissance.innerHTML = liste.model[i].dateNaissance;
		let cellAge = document.createElement('td');
		cellAge.innerHTML = getAge(liste.model[i].dateNaissance);
		let cellNum = document.createElement('td');
		cellNum.innerHTML = liste.model[i].numCI;
		let cellAction = document.createElement('td');
		let ActionModifier = document.createElement('button'); // Créer un élément <button>
		ActionModifier.type = 'button';
		ActionModifier.textContent = 'Modifier';
		ActionModifier.onclick = () => AjouterDansLeFormulaire(liste.model[i].idEtud);
		let ActionSupprimer = document.createElement("BUTTON");
		ActionSupprimer.type = 'button';
		ActionSupprimer.textContent = 'supprimer';
		ActionSupprimer.onclick = () => suppEtudiant(liste.model[i].idEtud);

		row_2.appendChild(cellNom);
		row_2.appendChild(cellPrenom);
		row_2.appendChild(cellDateNaissance);
		row_2.appendChild(cellAge);
		row_2.appendChild(cellNum);
		row_2.appendChild(cellAction);
		cellAction.appendChild(ActionModifier);
		cellAction.appendChild(ActionSupprimer);
		tbody.appendChild(row_2);
	}

}
fetchListe();

async function createStudent() { //creation du etudient
	let student = {
		"nom": document.getElementById("nom").value,
		"prenom": document.getElementById("prenom").value,
		"dateNaissance": document.getElementById("dateNaissance").value,
		"age": getAge(document.getElementById("dateNaissance").value),
		"num": document.getElementById("num").value
	};
	try {
		const response = await fetch('http://localhost:3000/create', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(student)

		});

		if (!response.ok) {
			throw new Error('Network response was not ok');
		}
		const createdStudent = await response.json();

		return createdStudent;
	} catch (error) {
		console.error(error);
	}


}

async function AjouterDansLeFormulaire(id) {// recupré l'information d'etudient mettre dans le formulaire
	const response = await fetch(url + '/index/' + id);
	const liste = await response.json();
	document.getElementById("id").value = liste.idEtud;
	document.getElementById("nom").value = liste.nom;
	document.getElementById("prenom").value = liste.prenom;
	document.getElementById("dateNaissance").value = liste.dateNaissance;
	document.getElementById("num").value = liste.numCI;
	console.log(liste.numCI);
	toggle();
}

async function editStudent() {// chenger mon base de donnée avec les nouveau information
	let id = document.getElementById("id").value;
	let student = {
		"nom": document.getElementById("nom").value,
		"prenom": document.getElementById("prenom").value,
		"dateNaissance": document.getElementById("dateNaissance").value,
		"age": getAge(document.getElementById("dateNaissance").value),
		"num": document.getElementById("num").value
	};
	try {
		const response = await fetch('http://localhost:3000/index/' + id, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(student)

		});

		if (!response.ok) {
			throw new Error('Network response was not ok');
		}
		const editStudent = await response.json();

		return editStudent;
	} catch (error) {
		console.error(error);
	}

	//fetchliste();
}

async function supprimerEtudiant(id) {//supprimer etudient
	try {
		const response = await fetch('http://localhost:3000/suppEtudiant/' + id, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			},

		});

		if (!response.ok) {
			throw new Error('Network response was not ok');
		}
		const suppEtudiant = await response.json();

		return suppEtudiant;
	} catch (error) {
		console.error(error);
	}
}
async function rechercheEtudiant() { //recherche dans mon base de donnée à un element
	let student = document.getElementById("recherche").value;
	try {
		const response = await fetch('http://localhost:3000/rechercheEtudient', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ recherche: student })

		});

		if (!response.ok) {
			throw new Error('Network response was not ok');
		}
		const rechercheEtudiant = await response.json();
		document.getElementById("tab").innerHTML = "";
		let table = document.createElement('table');
		let thead = document.createElement('thead');
		let tbody = document.createElement('tbody');

		table.appendChild(thead);
		table.appendChild(tbody);
		document.getElementById('tab').appendChild(table);
		let row_1 = document.createElement('tr');
		let heading_1 = document.createElement('th');
		heading_1.innerHTML = `nom<span onclick=sortTable(0,1)>&#9660;</span><span onclick=sortTable(0,0)>&#9650;</span>`;
		let heading_2 = document.createElement('th');
		heading_2.innerHTML = `Prenom<span onclick=sortTable(1,1)>&#9660;</span><span onclick=sortTable(1,0)>&#9650;</span>`;
		let heading_3 = document.createElement('th');
		heading_3.innerHTML = `Age<span onclick=sortTable(2,1)>&#9660;</span><span onclick=sortTable(2,0)>&#9650;</span>`;
		let heading_4 = document.createElement('th');
		heading_4.innerHTML = `Date de Naissance<span onclick=sortTable(3,1)>&#9660;</span><span onclick=sortTable(3,0)>&#9650;</span>`;
		let heading_5 = document.createElement('th');
		heading_5.innerHTML = `Numéro de la carte d'identité nationale<span onclick=sortTable(4,1)>&#9660;</span><span onclick=sortTable(4,0)>&#9650;</span>`;
		let heading_6 = document.createElement('th');
		heading_6.innerHTML = "Action";

		row_1.appendChild(heading_1);
		row_1.appendChild(heading_2);
		row_1.appendChild(heading_3);
		row_1.appendChild(heading_4);
		row_1.appendChild(heading_5);
		row_1.appendChild(heading_6);
		thead.appendChild(row_1);
		for (let i = 0; i < rechercheEtudiant.length; i++) {
			let row_2 = document.createElement('tr');
			let cellNom = document.createElement('td');
			cellNom.innerHTML = rechercheEtudiant[i].nom;
			let cellPrenom = document.createElement('td');
			cellPrenom.innerHTML = rechercheEtudiant[i].prenom;
			let cellDateNaissance = document.createElement('td');
			cellDateNaissance.innerHTML = rechercheEtudiant[i].dateNaissance;
			let cellAge = document.createElement('td');
			cellAge.innerHTML = getAge(rechercheEtudiant[i].dateNaissance);
			let cellNum = document.createElement('td');
			cellNum.innerHTML = rechercheEtudiant[i].numCI;
			let cellAction = document.createElement('td');
			let ActionModifier = document.createElement('button'); // Créer un élément <button>
			ActionModifier.type = 'button';
			ActionModifier.textContent = 'Modifier';
			ActionModifier.onclick = () => AjouterDansLeFormulaire(rechercheEtudiant[i].idEtud);
			let ActionSupprimer = document.createElement("BUTTON");
			ActionSupprimer.type = 'button';
			ActionSupprimer.textContent = 'supprimer';
			ActionSupprimer.onclick = () => suppEtudiant(rechercheEtudiant[i].idEtud);
			row_2.appendChild(cellNom);
			row_2.appendChild(cellPrenom);
			row_2.appendChild(cellDateNaissance);
			row_2.appendChild(cellAge);
			row_2.appendChild(cellNum);
			row_2.appendChild(cellAction);
			cellAction.appendChild(ActionModifier);
			cellAction.appendChild(ActionSupprimer);
			tbody.appendChild(row_2);
		}
	} catch (error) {
		console.error(error);
	}

}