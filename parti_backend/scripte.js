const { name } = require("ejs");
const express = require("express");
const path = require("path");
const sqlite3 = require("sqlite3").verbose();
const cors = require('cors')
// Création du serveur Express
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json())
// CORS

app.use(cors({
	origin: function (origin, callback) {
		console.log('ORIGIN ', origin)
		if (!origin) return callback(null, true);
		return callback(null, true);
	}
}));
// Configuration du serveur
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));
app.use(express.static(path.join(__dirname, "public")));
/////////////////////////
const db_name = path.join(__dirname, "data", "apptest.db");
const db = new sqlite3.Database(db_name, err => {
	if (err) {
		return console.error(err.message);
	}
	console.log("Connexion réussie à la base de données 'apptest.db'");
});
/////////////////////////
const sql_create = `CREATE TABLE IF NOT EXISTS "listeEtudient" (
	"idEtud"	INTEGER NOT NULL,
	"nom"	VARCHAR(50),
	"prenom"	VARCHAR(50),
	"dateNaissance"	DATE,
	"age" INTEGER,
	"numCI"	INTEGER,
	PRIMARY KEY("idEtud" AUTOINCREMENT)
);`;

/////////////////////////
db.run(sql_create, err => {
	if (err) {
		return console.error(err.message);
	}
	console.log("Création réussie de la table 'etudients'");
});

/////////////////////////

// Démarrage du serveur
app.listen(3000, () => {
	console.log("Serveur démarré (http://localhost:3000/) !");
});
/////////////////////////
// GET /
app.get("/", (req, res) => {
	// res.send("Bonjour le monde...");
	res.render("Accueil", { model: "" });
});
/////////////////////////

//Get /data/1?recherche =?
//data/:page
app.get("/data/:page", (req, res) => {
	let recherche = req.query.recherche;
	let rowsPerPage = 5;
	let page = req.params.page;
	let offset = (page - 1) * rowsPerPage;
	const sql = "SELECT * FROM listeEtudient WHERE nom = ? OR prenom =? OR dateNaissance=? OR numCI=? ORDER BY prenom  LIMIT ? OFFSET ? ";
	db.all(sql, [recherche, recherche, recherche, recherche, rowsPerPage, offset], (err, rows) => {
		if (err) {

			return console.error(err.message);
		}

		//res.render("data", { model: rows ,page:parseInt(page),recherche:recherche});
		res.status(200).json(rows)
	});
});
//////////////
//Get /liste de touts la Base de données
app.get("/liste", (req, res) => {

	const sql = "SELECT * FROM listeEtudient ORDER BY prenom";
	db.all(sql, [], (err, rows) => {
		if (err) {
			console.log("hi page");
			return console.error(err.message);
		}
		//res.render("liste", { model: rows });
		res.status(200).json({ model: rows });
	});
});
/////////////////////////
function getAge(date) { //Convertir la date de naissance en âge.

	var diff = Date.now() - new Date(date).getTime();
	var age = new Date(diff);
	return Math.abs(age.getUTCFullYear() - 1970);
}
/////////////////////////
// GET /create
app.get("/create", (req, res) => {
	res.render("create", { model: {} });
});
/////////////////////////
// POST /create
app.post("/create", (req, res) => {
	const sql = "INSERT INTO listeEtudient(nom,Prenom,dateNaissance,age,numCI) VALUES (?, ?, ?,?,?)";
	const age = getAge(req.body.dateNaissance);

	const etudiant = [req.body.nom, req.body.prenom, req.body.dateNaissance, age, req.body.num];
	db.run(sql, etudiant, (err) => {

		if (err) {
			return console.error(err.message);

		}

	});
	//res.redirect('/liste');  
	res.status(200).json({ message: "ligne etait créé" });

});
/////////////////////////
// GET /edit/5
app.get("/index/:id", (req, res) => {
	const id = req.params.id;
	const sql = "SELECT * FROM listeEtudient WHERE idEtud = ?";
	db.get(sql, id, (err, row) => {
		if (err) {

			return console.error(err.message);
		}
		//res.render("index", { model: row });
		res.status(200).json(row)
	});
});
/////////////////////////
// POST /edit/5
app.post("/index/:id", (req, res) => {
	const id = req.params.id;
	const age = getAge(req.body.dateNaissance);
	const etudient = [req.body.nom, req.body.prenom, req.body.dateNaissance, age, parseInt(req.body.num), id];
	const sql = "UPDATE listeEtudient SET nom = ?, prenom = ?, dateNaissance = ? ,age=?,numCI = ? WHERE (idEtud = ?)";
	db.run(sql, etudient, err => {
		if (err) {
			return console.error(err.message);
		}
		//res.redirect("/liste");
		res.status(200).json()
	});
});
/////////////////////////
app.get("/suppEtudiant/:id", (req, res) => {

	const sql = "DELETE from listeEtudient WHERE idEtud = ?";
	const sql2 = "UPDATE listeEtudient SET idEtud = idEtud - 1 WHERE idEtud > ?";
	const params = [req.params.id];

	db.run(sql, params, (err) => {

		db.run(sql2, params, (err) => {

			if (err) {
				return console.error(err.message);

			}

			if (err) {
				return console.error(err.message);

			}

		});
	});
	//res.redirect('/liste');  
	res.status(200).json();
});
/////////////////////////
app.post("/rechercheEtudient", (req, res) => {

	const sql = "SELECT * FROM listeEtudient WHERE nom = ? OR prenom =? OR dateNaissance=? OR numCI=? ";
	const params = [req.body.recherche, req.body.recherche, req.body.recherche, req.body.recherche];
	db.all(sql, params, (err, rows) => {

		if (err) {

			return console.error(err.message);

		}
		// res.render("/data", { model: rows });
		res.status(200).json(rows)
	});



});
   /////////////////////////

